from collections import defaultdict
import csv
from datetime import datetime as dt
import matplotlib.dates as md
import matplotlib.pyplot as plt

t_series = []
b_series = defaultdict(list)
with open('update-history.csv', 'r') as csvfile:
    for t, *line in csv.reader(csvfile):
        t_series.append(md.date2num(dt.fromisoformat(t)))
        for bc in line:
            b_str, c_str = bc.split(': ')
            c = int(c_str)
            b_series[b_str].append(c)

print(t_series)
print(b_series)
print(b_series['ALL'])

plt.style.use('bmh')
plt.rcParams['font.family'] = 'Nimbus Sans'

fig, ax = plt.subplots()
ax2 = ax.secondary_yaxis('right', functions=(lambda x: x*40., lambda x: x/40.))
plt.xticks(rotation=30)
xfmt = md.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(xfmt)

t_series.append(md.date2num(dt.utcnow()))

for brand, cnt in b_series.items():
    cnt.append(cnt[-1])
    if brand == 'ALL':
        cnt = [e/40. for e in cnt]
    plt.step(t_series, cnt, where='post', label=brand)

plt.legend(ncols=(len(b_series)//2), bbox_to_anchor=(0, 1),
           loc='lower left', fontsize='small')
plt.show()

