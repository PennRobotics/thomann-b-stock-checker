VERBOSE = True
USE_COLORS = True

from datetime import datetime
import sys
import time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By

BLUE = '\033[36;1m' if USE_COLORS else ''
YELLOW = '\033[33m' if USE_COLORS else ''
RESET = '\033[0m' if USE_COLORS else ''


def run_tests(url_list):
    assert type(url_list) == list
    n_url = len(url_list)
    if n_url == 0:
        return 0

    if VERBOSE:
        print(f'Checking {n_url} retail URL{"s" if n_url > 1 else ""}...', file=sys.stdout, flush=True)

    options = FirefoxOptions()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options)
    driver.implicitly_wait(5)

    consent_given = False
    for url in url_list:
        driver.get(url)
        time.sleep(3)

        if not consent_given:
            consent_button = driver.find_elements(by=By.CLASS_NAME, value='consent-button')
            if consent_button:
                consent_button[0].click()
            consent_given = True

        if 'Page not found' in driver.title:
            print(f'WARNING: 404 while opening {url}', file=sys.stderr, flush=True)
            continue

        if not driver.find_elements(by=By.CLASS_NAME, value='keyfeatures'):
            print(f'An error has occurred while opening {url}', file=sys.stderr, flush=True)
            return 1

        if VERBOSE:
            prod_title = driver.title.split('–', 1)[0]
            print(f'- {prod_title}', file=sys.stdout, flush=True)

        has_bstock = driver.find_elements(by=By.LINK_TEXT, value='B-Stock')
        if has_bstock:
            if VERBOSE:
                print(YELLOW+f'B-Stock: {has_bstock[0].get_attribute("href")}'+RESET, file=sys.stderr, flush=True)
            return 1

    if VERBOSE:
        print(BLUE+'All URLs checked, no B-Stock available'+RESET, file=sys.stdout, flush=True)
    return 0


if __name__ == '__main__':
    with open('LAST-RESTOCK-TIMESTAMP.txt', 'r') as ts_restock_file:
        ts_restock = ts_restock_file.readlines()[0]
    with open('LAST-COMPLETE-GATHER-TIMESTAMP.txt', 'r') as ts_data_gather_file:
        ts_data_gather = ts_data_gather_file.readlines()[0]

    if ts_data_gather < ts_restock:
        with open('page-check.txt', 'r') as url_file:
            url_list = url_file.readlines()

        e_code = run_tests(url_list)
        with open('LAST-COMPLETE-GATHER-TIMESTAMP.txt', 'w') as ts_data_gather_file:
            print(str(datetime.utcnow()), file=ts_data_gather_file)
        time.sleep(0.5)  # Provide enough time for pipeline to catch stderr/stdout

    else:
        if VERBOSE:
            print(BLUE+'An inventory update has not occurred yet.'+RESET, file=sys.stdout, flush=True)
        e_code = 0

    sys.exit(e_code)
