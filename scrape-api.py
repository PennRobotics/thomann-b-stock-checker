USE_COLORS = True
BASE_URL = "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT"
MFG_LIST = ["", "Shure", "K&M", "Yamaha", "Allen & Heath", "DPA", "Latch Lake", "Triad-Orbit", "Rycote", "Vater"]
BONUS_MFG_LIST = ["Schoeps", "Mach One", "TritonAudio", "SE Electronics"]
BONUS_URLS = [("Yamaha Silent Brass", "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=BLZDSB&filter=true"),
              ("Sennheiser Tom Mic", "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&manufacturer[]=Sennheiser&gk=MIINDRTO&filter=true"),
              ("GUITAR", "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=gi&filter=true"),
              ("DRUM",   "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=dr&filter=true"),
              ("KEYS",   "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=ta&filter=true"),
              ("STUDIO", "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=st&filter=true"),
              ("S/W",    "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=sw&filter=true"),
              ("LIVE",   "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=pa&filter=true"),
              ("LIGHT",  "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=li&filter=true"),
              ("DJ",     "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=dj&filter=true"),
              ("VID",    "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=vi&filter=true"),
              ("MIC",    "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=mi&filter=true"),
              ("FX",     "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=sp&filter=true"),
              ("BRASS",  "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=bl&filter=true"),
              ("TRAD",   "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=tr&filter=true"),
              ("CASE",   "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=ca&filter=true"),
              ("ACCY",   "https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[]=BLOWOUT&gk=zu&filter=true")]

import csv
from datetime import datetime
import json
from json.decoder import JSONDecodeError
import requests
import sys

BLUE = '\033[36;1m' if USE_COLORS else ''
YELLOW = '\033[33m' if USE_COLORS else ''
RESET = '\033[0m' if USE_COLORS else ''

escape = lambda st: st.replace("&", "%26")
make_urls = lambda mfg_list: tuple([(BASE_URL + ("&manufacturer[]=" if len(mfg) else ""), mfg) for mfg in mfg_list])


try:
    infile = open('LAST-COUNTS.txt', 'r')
    last_count = json.load(infile)
    infile.close()
    file_exists = True
except (IOError, JSONDecodeError):
    last_count = {}
    file_exists = False
except Exception as e:
    print('Possible API error', sys.exc_info()[0])
    raise e

change_detected = False

purge_list = set(last_count) - set(MFG_LIST)
for elem in purge_list:
    change_detected = True
    del last_count[elem]

print('Current Unique Products:')
for url_to_mfg, mfg in make_urls(MFG_LIST):
    r = requests.get(url_to_mfg + escape(mfg))
    cnt = r.json()["gtmSearchResponse"]["parameters"]["result_count"]
    if file_exists and (mfg not in last_count or cnt != last_count[mfg]):
        change_detected = True
    last_count[mfg] = cnt
    mfg = mfg if mfg else "ALL"
    print(f'  {mfg}: {cnt}')

if not file_exists or change_detected:
    with open("LAST-COUNTS.txt", "w") as outfile:
        outfile.write(json.dumps(last_count))
else:
    print(YELLOW + "Looks like no changes" + RESET)
    sys.exit(0)

# Temporary watchlist (leaves CSV history intact)
#   (down here to reduce server load when all categories are polled)
print('^v^v^v^v^v^v^v^v^')
for url_to_mfg, mfg in make_urls(BONUS_MFG_LIST):
    r = requests.get(url_to_mfg + escape(mfg))
    cnt = r.json()["gtmSearchResponse"]["parameters"]["result_count"]
    print(f'  {mfg}: {cnt}')
for descr_and_url in BONUS_URLS:
    descr, full_url = descr_and_url
    r = requests.get(full_url)
    cnt = r.json()["gtmSearchResponse"]["parameters"]["result_count"]
    print(f'  {descr}: {cnt}')

cur_time = str(datetime.utcnow())
with open("LAST-RESTOCK-TIMESTAMP.txt", "w") as timestampfile:
    print(cur_time, file=timestampfile)
    print(BLUE + f"Restock timestamp recorded: {cur_time}" + RESET)

output_row = [cur_time]
for k, v in last_count.items():
    v = v if k in MFG_LIST else -1
    output_row.append(f'{k if len(k) else "ALL"}: {v}')

with open("update-history.csv", "a") as historyfile:
    historywriter = csv.writer(historyfile)
    historywriter.writerow(output_row)

# -------- Check for each item in wishlist.csv --------
# TODO

mfg_data = {}

### with csv.reader('wishlist.csv') as wishfile:
###     wishfile.next()
###     for row in wishfile:
###         # RETAIL_ITEM_NUM, B_STOCK_ITEM_NUM, MANUFACTURER_URL, COMMENT
###         retail_num, b_num, mfg, _ = row
###         if mfg not in mfg_data:  # TODO: and size of dict entry < reported item count (e.g. multiple pages)
###             r = requests.get(url_to_mfg + mfg)
###             data = r.json()["articleListsSettings"]["articles"]
###             mfg_data[mfg] = data
###         else:
###             data = mfg_data[mfg]
###         for article in data:
###             print('E')
###             print(article)
###             # TODO: for each line, poll manufacturer from dict or requests.get() and check if any ### image filename matches the line.
###             # TODO: If so, we need to fail so an email is sent from Gitlab
###             # TODO: In the failing case, save the found B-stock items in an artifact.
