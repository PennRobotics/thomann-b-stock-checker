from dataclasses import dataclass
from datetime import datetime
import json
from random import randint
from random import random
import requests
import sys
import time

DOWNLOAD = False
USE_COLORS = True
THROTTLE_REQS = True
BIG_SCRAPE = False
START_PAGE = 0

if BIG_SCRAPE:
    BASE_URL = 'https://www.thomann.de/de/search_searchAjax.html?oa=ala&ls=100&pg={page}'
    FILENAME = 'all-items.json'
else:
    BASE_URL = 'https://www.thomann.de/de/search_searchAjax.html?oa=ala&ls=100&pg={page}&marketingAttributes[0]=BLOWOUT'
    FILENAME = 'b-stock-items.json'

BLUE = '\033[36;1m' if USE_COLORS else ''
YELLOW = '\033[33m' if USE_COLORS else ''
RESET = '\033[0m' if USE_COLORS else ''
debug = print if 1 else lambda *a, **b: None
__thomann_mfgs = ['Thomann',
                  'Harley Benton',
                  'Fun Generation',
                  'Startone',
                  'Lead Foot',
                  'Millenium',
                  'Flyht Pro',
                  'Roth & Junius',
                  'Stairville',
                  'Swissonic',
                  'Zultan',
                  'DrumCraft',
                  'Stageworx',
                  'Roadworx',
                  'Varytec',
                  'Scala Vilagio',
                  'Botex',
                  'Sirus',
                  'Ignition',
                  'Syrincs',
                  'Echolette',
                  'Hemingway',
                  'the box',
                  'the box pro',
                  'the sssnake',
                  'pro snake',
                  't.akustik',
                  'the t.amp',
                  'the t.bone',
                  'the t.mix',
                  'the t.pc',
                  'the t.racks',
                  'Thon']


@dataclass
class Item:
    mfg: str
    name: str
    a_id: int
    price: float
    num_ratings: int
    avg_rating: float
    category: str = ''


if DOWNLOAD:
    pg_url = BASE_URL.replace('{page}', '1')
    debug(f'First request: {pg_url}')

    r = requests.get(pg_url)
    num_pages = r.json()['pagingSettings']['lastPage']
    debug(f'Pages: {num_pages}')

items = {}

if 0:
    # Load existing file into `items`, for appending to a part-full database
    with open(FILENAME, 'r') as infile:
        j = json.load(infile)
    for k, v in j.items():
        items[int(k)] = Item(v['mfg'],
                             v['name'],
                             v['a_id'],
                             v['price'],
                             v['num_ratings'],
                             v['avg_rating'],
                             v['category'] if 'category' in v else '')

if DOWNLOAD:
    for page in map(lambda n: n + 1, range(START_PAGE, num_pages)):
        if page % 10 == 1:
            est = (1 + num_pages - page) // 4  # avg 15 seconds per page
            print(f'~{est}m left  \tScrape to pg {num_pages}, at ', end='')
        print(f' {page}', end='')
        if page % 10 == 0:
            print('')
        else:
            print(',', end='')
        sys.stdout.flush()

        if THROTTLE_REQS:
            time.sleep((7 if random() > 0.05 else 24) + randint(0, 12))

        pg_url = BASE_URL.replace('{page}', str(page))
        r = requests.get(pg_url)

        article_list = r.json()['articleListsSettings']['articles']
        for article in article_list:
            if not BIG_SCRAPE and not article['isBstock']:
                continue

            pid = int(article['id'])
            mfg = article['manufacturer']
            name = article['model']
            if BIG_SCRAPE:
                try:
                    a_id = int(article['mainImage']['fileName'][0:6])
                except KeyError:
                    a_id = int(article['number'])
            else:
                a_id = article['aStockArticleId']
            price = round(float(article['price']['primary']['rawPrice']), 2)
            nr_r = article['rating']['count']
            av_r = article['rating']['rating']
            category = article['ecommerceEventItem']['item_category']

            items[pid] = Item(mfg, name, a_id, price, nr_r, av_r, category)
    print('end')

    with open(FILENAME, 'w') as outfile:
        outfile.write(json.dumps(items, default=vars))

    if BIG_SCRAPE:
        cur_time = str(datetime.utcnow())
        with open('LAST-BIGSCRAPE-TIMESTAMP.txt', 'w') as timestampfile:
            print(cur_time, file=timestampfile)
            print(BLUE + f'Big scrape timestamp recorded: {cur_time}' + RESET)

if not DOWNLOAD:
    with open('b-stock-items.json', 'r') as infile:
        j = json.load(infile)
    for k, v in j.items():
        items[int(k)] = Item(v['mfg'],
                             v['name'],
                             v['a_id'],
                             v['price'],
                             v['num_ratings'],
                             v['avg_rating'],
                             v['category'] if 'category' in v else '')

# Now filter items according to sort strategy and list
if not BIG_SCRAPE:
    # Load full inventory into memory
    with open('all-items.json', 'r') as infile:
        j = json.load(infile)
    inven = {}
    for k, v in j.items():
        inven[int(k)] = Item(v['mfg'],
                             v['name'],
                             v['a_id'],
                             v['price'],
                             v['num_ratings'],
                             v['avg_rating'],
                             v['category'] if 'category' in v else '')

    if 0:  # Get average rating across entire inventory, exit
        numer_wt = 0  # weighted
        denom_wt = 0
        numer_uw = 0  # unweighted
        denom_uw = 0
        for item in inven.values():
            if not item.avg_rating:
                continue
            numer_wt += item.avg_rating * item.num_ratings
            denom_wt += item.num_ratings
            numer_uw += item.avg_rating
            denom_uw += 1
        print(f'Avg of all ratings: {round(numer_wt/denom_wt, 3)}, ' +
              f'Avg prod. rating (unweighted): {round(numer_uw/denom_uw, 3)}')
        sys.exit(0)

    if 0:  # Get average rating of each manufacturer, exit
        mfg_sort = []
        mfg_w_sort = []
        current_mfg = ''
        for item in inven.values():
            if item.mfg != current_mfg:
                if current_mfg != '':
                    denom = max(denom, 1)
                    av = round(numer / (2 * denom), 3)
                    mfg_sort.append([av, current_mfg])
                    mfg_w_sort.append([numer, current_mfg])

                current_mfg = item.mfg
                numer = 0
                denom = 0

            if not item.avg_rating:
                continue

            numer += item.num_ratings * item.avg_rating
            denom += item.num_ratings

        with open('mfgs.txt', 'w') as fh:
            for r, name in sorted(mfg_sort):
                if r == 0.0 or r == 5.0:  # extremes first
                    print(f'{name}: {r}', file=fh)
            for r, name in sorted(mfg_sort):
                if r == 0.0 or r == 5.0:  # do not show extremes
                    continue
                print(f'{name}: {r}', file=fh)

        with open('mfgs-wt.txt', 'w') as fh:
            for r, name in sorted(mfg_w_sort):
                print(f'{name} ({r})', file=fh)

        sys.exit(0)

    if 0:  # Get average rating of any manufacturer and exit
        with open('mfgs.txt', 'r') as mfg_file:
            mfg_list = list(map(str.strip, mfg_file.readlines()))
        cumul = 0.0
        count = 0
        for row in mfg_list:
            _, rating_str = row.split(': ')
            rating = float(rating_str)
            if rating == 0:
                continue
            cumul += rating
            count += 1
        print(f"Avg manufacturer gets a {round(cumul/count,3)} rating")
        sys.exit(0)

    w_sort = []
    pure_rating_sort = []
    for b_item in items.values():
        try:
            a_item = inven[b_item.a_id]
        except KeyError:
            continue

        # Combine ratings
        total_ct = a_item.num_ratings + b_item.num_ratings
        if b_item.num_ratings == 0:
            b_item.avg_rating = 0
        if a_item.num_ratings == 0:
            a_item.avg_rating = 0
        if 1:  # Fill in missing ratings
            nf = 5  # number to fill
            __avg_assumed_rating = 9.1
            nr_to_fill = max(0, nf - (b_item.num_ratings + a_item.num_ratings))
            total_ct = max(total_ct, nf)
            num_times_rating = (a_item.num_ratings * a_item.avg_rating +
                                b_item.num_ratings * b_item.avg_rating +
                                nr_to_fill * __avg_assumed_rating)
        else:
            total_ct = max(total_ct, 1)
            num_times_rating = (a_item.num_ratings * a_item.avg_rating +
                                b_item.num_ratings * b_item.avg_rating)
        avg_ab_rating = num_times_rating / total_ct

        w_sort.append([num_times_rating,
                       avg_ab_rating,
                       f'{a_item.mfg} {a_item.name}'])
        pure_rating_sort.append([avg_ab_rating,
                                 total_ct,
                                 f'{a_item.mfg} {a_item.name}'])

    print('Sort by number and quality of ratings:')
    for n, (val, avg_rate, name) in enumerate(sorted(w_sort, reverse=True)):
        if n == 20:
            break
        print(name, round(avg_rate / 2, 3), f'({int(val)})')

    print('\nSort by quality of ratings:')
    n = 0
    for avg_rate, cnt, name in sorted(pure_rating_sort, reverse=True):
        if 1:  # exclude perfect ratings
            if avg_rate == 10:
                continue
        n += 1
        if n >= 20:
            break
        print(name, round(avg_rate / 2, 3), cnt)

    # Sort by biggest markdown
    pct_sort = []
    for b_item in items.values():
        try:
            a_item = inven[b_item.a_id]
        except KeyError:
            continue

        # Combine ratings
        pct_sort.append([b_item.price / a_item.price,
                         b_item.price,
                         a_item.mfg,
                         a_item.name])

    print('\nSort by biggest discount percentage:')
    for n, (pct, b_price, mfg, name) in enumerate(sorted(pct_sort)):
        if n == 20:
            break
        print(round(b_price, 2), f' ({round(100*pct, 1)}%)', mfg + " " + name)

    print('\nSort by biggest discount percentage, no Thomann brands:')
    n = 0
    for pct, b_price, mfg, name in sorted(pct_sort):
        if mfg in __thomann_mfgs:
            continue
        if n == 20:
            break
        print(round(b_price, 2), f' ({round(100*pct, 1)}%)', mfg + " " + name)
        n += 1

    # No equivalent A-stock
    print('\nCloseouts with rating:')
    if 1:
        for b_item in items.values():
            try:
                a_item = inven[b_item.a_id]
            except KeyError:
                if b_item.num_ratings:
                    print(b_item)

    # No equivalent A-stock
    print('\nCloseouts, some categories excl., no house brands, under 600:')
    if 1:
        for b in sorted(items.values(), key=lambda i: i.category):
            try:
                a_item = inven[b.a_id]
            except KeyError:
                if b.category not in ['BL', 'GI', 'LI', 'SY'] and \
                   b.mfg not in __thomann_mfgs and \
                   b.price < 600:
                    print(f'[{b.category}] {b.mfg} {b.name} - {b.price}')
