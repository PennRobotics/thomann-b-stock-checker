newidea
=======

_Can we do better?_

## Problems with B-stock sorting

While it is possible (and fun) to browse the sale rack in its entirety, there are drawbacks to every sort method:

- Popularity: non-sale items are often included* near the top, and the list is skewed toward specific products e.g. guitars
- Price lo-to-hi: this one is genuinely interesting; guitar pedals, stands, drum kit pieces, fun instruments, and accessories
- Price hi-to-lo: are you looking to save nearly 2000 euros on a new Tuba? a 56-stop pipe organ? Neumann U67?
- Best rated: not enough data points**
- New products: a more interesting list; good mix of categories, and recent products usually improve on existing designs
- A to Z: one in the least useful pair of sort methods of the bunch
- Z to A: another in the least useful pair of sort methods of the bunch

\* &ndash; _Doughty_ A-stock products have been on the B-stock list for about half a year now. Either Thomann devs do
  not know there is a bug (likely as there are other visible bugs in Firefox), they know there is a bug and do not care,
  or there is some intentional reason this strange product category (lighting hardware) gets included in discounts
  despite not actually being discounted. Similarly, software and music stands often end up here even while the B-stock
  variant of the same music stand is also listed.

\** &ndash; About ten percent of discount items have a rating. Nearly _all_ of their non-discounted peers have a rating. Worse,
  the discount item might have a single rating of 5 stars while its full priced sibling has less than 4 stars on multiple reviews.
  _Best rated_ would be the best sort method **if** it included all ratings for that item.

I would expect to see known great deals like Schoeps, Sound Devices, URSA, Dragonfly, Kolberg, and Latch Lake to rise to the top
while existing five-star B-stock items like the singly-rated AH 803GP Eb Alto Horn to shrink into obscurity due to low linked ratings.
There is nothing inherently special about the Magic FX Power Drop, the newest discounted product as of today, with no ratings
at all. Close behind are a Boss distortion pedal and Rode wireless mic with a few good A-stock ratings. Those should list slightly
ahead of the Magic FX. You have to go quite a way back until you find a parent product with 5 ratings, but this might be the MMX 200
headset with a rating of 4.2. So, that clearly should not lead the front of a good sort method.

I believe there is the opportunity to analyze all of the information available via the API and create much better B-stock sort methods:

### Average number of ratings

First, I would want to plot product age, cost, and number of ratings on a 3d plot. I think by fitting some simple surface, you
can immediately start to see if a product is hot or not. Of course, people are more likely to buy a 100 dollar SM58 than a
Sennheiser Profipower costing five times as much, so you would expect many more reviews for cheaper products within a category.
This isn't perfect, but it's a good way of determining how many ratings a product of certain age _should_ have.

Unfortunately, product age can only be found on individual product pages and _not_ via the search API.

How many ratings a product _should_ have is not such a critical piece of information on its own, but it should confirm that
the popularity metric is accurate or at least that users are more or less opinionated than you would expect.

### How often does a brand have discounted products?

For large brands, this is easy. Open the brand page, divide number of price-slashed items by the number of items available, average
that value over a few days; done!

For smaller or more expensive brands (aka lower volume) you need some sort of proxy. This is maybe where it is helpful knowing the
expected number of ratings for a product's age and cost, which would indicate whether they sell more or less than a comparable item
from a major manufacturer. Then, you can start to build a picture of how many discounted items would be normal and whether that
brand is far over or far under.

If Shure has 60 sale items from a selection of 900 products, a small microphone maker should have 4 discounts on 60 items but only
as long as the products have been similarly priced and not very recently released. (_Austrian Audio_ has 4 discounts on 43 items,
but 3 of these are headphones. This suggests to me they should stick to microphones.)

Here is the question: If a brand rarely has discounted products despite being in the same price range and general availability (and
especially if the full priced items have plenty of ratings) would you expect them to have a better product? Sure, it makes sense
that expensive brands of microphones (Neumann, Coles, Schoeps, AEA) are rarely discounted and have extreme prices, but if you want
to choose between a stereo pair of SDC mics for between 350 and 400 euros, you should also consider how many people are returning
an SE8 set, a Universal Audio set, an NT5 pair, Haun, and any other product falling into this category.

If the Rode NT55 costs the same as a WA-84 but has 10 times as many reviews, how much is because of the product age of the Rode?
Similarly, if the NT55 is discounted once every three weeks but the Warm Audio only twice per year, is that because there are less
sold of the Warm? Or less returned? Do you buy a rarely discounted product with 4.6 stars over a frequently returned, similarly
priced item with 4.8 stars? What if there is a price difference?

The aim is to put these factors on equal footing. Thus, by knowing how often a manufacturer has discounted products, you can decide
to amplify the effects of their product rating over whatever the average rating is for all products.

As a final warning, you would not want to take a single 3-star rating for a product and declare that product worthless. The more
ratings, the closer you get to accurate. Similarly, if a manufacturer has a single product and that product is on sale (pipe organ,
harp, other specialty items) you might be looking at an epic closeout deal.

### Linked ratings

I think this would be the most immediately helpful metric. For each B-stock product, add to it the ratings from its normal price
listing. If a product is new, add enough "totally average" ratings to get it to 5, sort by product age, multiply by some shift
factor to list more important if a product has more good ratings than expected and less important if more bad ratings, and then
perhaps sort so less expensive items are closer to the front but the best expensive items are still listed alongside. Finally,
filter out the "actually not discounted" cruft.

Unfortunately, one improvement that would be missing from this strategy is products as part of a series. If a product comes in
five colors but are otherwise identical, you should be able to sum the ratings across products. There might be few enough that
you could do this manually, but an automatic method would be a vast improvement.

### Filtering away

Excluding individual product categories would also be a great way to improve a custom sort algorithm. I know Taylor, Martin,
Lakewood, and even Yamaha are putting out some truly fine guitars. It is even possible that if you take all the ratings at each
price level for all guitar builders, some lesser-known builder is making equally nice guitars at a lower price. But I am not
in the market for a guitar. A filter to remove guitars and basses would take away 40 percent of the B-stock listings.

Granted, some of these products are things like barstools, instrument tuners, special-purpose microphones, and cleaning supplies.
I think the API has a field for primary category and then additional fields for cross-listed products, but I would need to explore.

Unfortunately, I cannot find a good way to get JSON results directly for a given A-stock ID (which is different than product ID)
so I created a scraper to gather the entire website inventory. (Sorry, Thomann!)


## Summary

We want to know what is expected for all products as well as for a hypothetical brand and any hypothetical product. Then, this
can be used to estimate a product's true value even with limited knowledge and then sort by this value.


## Findings

Sorting manufacturers by review strength (number of reviews times average review score) yields expected results. Thomann store
brands lead this list: Harley Benton, Thomann, Millenium, sssnake, pro snake, Stairville, t.bone, Thon, etc. Among these are
that dreaded copycat/ripoff company Behringer. Also, Cordial, K&M, and Neutrik. Guitars and string/pick providers are up here:
Fender, Dunlop, Daddario, Ernie Ball, Elixer, Remo. Boss is found up here. Not far behind? Gibson, Ibanez, Yamaha.

So these are the monopoly brands. What about average review score?

I filtered out perfect scores, but there is one that I feel has deserved their place here: Latch Lake. Impossibly expensive and
impossible to destroy during regular use, they are the cream of the crop when it comes to microphone stands.

I will admit to never hearing of AJH Synth before. They have about a dozen nearly universally loved panel-mount audio modules.
Another module maker near the head of the pack is Schlappi Engineering, followed closely by Der Mann mit der Maschine.

Next, Reutlinger. This looks like a special-purpose go-to brand for touring professionals. Also universally loved, they make
cable holders with safety compliance standards.

Then, Bitwig, HESU (they make guitar cabs and speakers), Noble & Cooley (snare drum maker), MG Mallets, Elysia (hardware effects),
WES Audio (hardware effects), and Ufip (cymbal maker)

Maybe because I am American, I assumed Ufip was just another cymbal brand. Evidently, some people absolutely love their sound
over the more popular brands.

Many of the findings up here speak for themselves: Rupert Neve/Neve, AEA, FabFilter, Manley, Greg Black, Courtois, Coles, Kolberg,
Trick Drums, Genelec, Mesa Boogie, Marcus Bonna, Schagerl, Schoeps, Soyuz, Manfrotto, Neutrik, and more. What is now interesting
is to find those Thomann brands among this list: sssnake is below average, t.bone is far below average (near Millenium), Thon is a
bit better at nearly 4.7. Aquarian and Sennheiser are next to each other.

Average, by the way, is **4.586** for a manufacturer.

I wonder if it makes sense to filter out house brands?

### B-stock output, sorting now by combined rating

```text
Strymon Ojai Expansion Kit, rating 4.98 with 51 reviews
Remo Bahia Buffalo Drum 16"x3,5", rating 4.974 with 39 reviews
Audio-Technica AE 5400, rating 4.971 with 34 reviews
Peter Hess phKSA-SET, rating 4.964 with 28 reviews
Global Truss F34C30 90° Corner, rating 4.963 with 27 reviews
Fender Pro Junior IV, rating 4.963 with 27 reviews
Thon Case Pioneer CDJ-3000, rating 4.958 with 24 reviews
Global Truss F33C24 90° Corner, rating 4.958 with 24 reviews
DiMarzio DP223BK PAF 36th Anniversary, rating 4.952 with 21 reviews
Allen & Heath M-DANTE-A card, rating 4.952 with 21 reviews
Markbass Little Mark IV, rating 4.947 with 19 reviews
Mojotone 59 Clone Humbucker Set N, rating 4.944 with 36 reviews
Coles 4038 Studio Ribbon, rating 4.943 with 35 reviews
Evh 5150 III 50 W 6L6 Head Stealth, rating 4.941 with 34 reviews
Schaller BMF Bass Tuner Set 4L C, rating 4.941 with 17 reviews
Neumann MT 48, rating 4.941 with 17 reviews
DW 5000TD4 Bass Drum Pedal, rating 4.938 with 80 reviews
RME ADI-2 Pro FS R Black Edition, rating 4.938 with 32 reviews
DiMarzio DP 107FBK Mega Drive F-Spaced, rating 4.938 with 32 reviews
```

The bass pedal has no reviews on its B-stock page but seems to be an excellent value at a discount.

I never considered an Audio-Technica condenser microphone for a handheld singing mic, but the rating speaks for itself.

Allen & Heath, Coles, and RME are all known for their quality and are in this list

This is clearly _already_ a better method of sorting B-stock items.

### Script output

```text
Sort by number and quality of ratings:
beyerdynamic DT-770 Pro 250 Ohm 4.769 (49649)
Rode NT1-A Complete Vocal Recording 4.733 (46821)
Harley Benton PowerPlant 4.264 (41033)
Millenium KS-1010 Black 4.459 (34769)
Shure SE215-CL 4.517 (25391)
the box MA120 MKII 4.338 (24875)
Thomann E-Guitar Case ABS 4.626 (24433)
Thomann E-Guitar Case Tweed 4.409 (21561)
AKG K-240 Studio 4.547 (20525)
Harley Benton TE-52 NA Vintage Series 4.64 (19077)
Thomann Western Guitar Case 4.48 (18736)
Shure SM 7 B 4.87 (18409)
Audio-Technica ATH-M50X 4.749 (16773)
Harley Benton D-120CE BK 4.371 (15735)
Harley Benton G112 Celestion V30 4.793 (15125)
Rode PSA-1 4.661 (14803)
Millenium KB-2006 Keyboard Bench 4.373 (14789)
Harley Benton SpaceShip 40 4.795 (14575)
Yamaha HS 7 4.812 (14281)
Behringer UMC404HD 4.616 (14253)

Sort by quality of ratings:
Strymon Ojai Expansion Kit 4.98 51
Remo Bahia Buffalo Drum 16"x3,5" 4.974 39
Audio-Technica AE 5400 4.971 34
Peter Hess phKSA-SET 4.964 28
Global Truss F34C30 90° Corner 4.963 27
Fender Pro Junior IV 4.963 27
Thon Case Pioneer CDJ-3000 4.958 24
Global Truss F33C24 90° Corner 4.958 24
DiMarzio DP223BK PAF 36th Anniversary 4.952 21
Allen & Heath M-DANTE-A card 4.952 21
Markbass Little Mark IV 4.947 19
Mojotone 59 Clone Humbucker Set N 4.944 36
Coles 4038 Studio Ribbon 4.943 35
Evh 5150 III 50 W 6L6 Head Stealth 4.941 34
Schaller BMF Bass Tuner Set 4L C 4.941 17
Neumann MT 48 4.941 17
DW 5000TD4 Bass Drum Pedal 4.938 80
RME ADI-2 Pro FS R Black Edition 4.938 32
DiMarzio DP 107FBK Mega Drive F-Spaced 4.938 32

Sort by biggest discount percentage:
53.0  (34.2%) Yellowtec Litt Signal Light YT9100 Base
105.0  (41.2%) Turbosound NUQ102-FY
1099.0  (42.0%) UNiKA NBB-1616e
185.0  (46.5%) LD Systems Roadman Slave
3398.0  (48.5%) JB-Lighting Sparx12 RGBW
339.0  (52.2%) Pearl Decade Maple Add-On Pack BB
23.3  (53.0%) Harley Benton Ersatzlautsprecher HBW-35
24.0  (53.3%) Eurolite Diffuser Cover 40° Multiflood
259.0  (54.1%) Ignition Stagepix Brain IP
47.0  (55.3%) Thon Back Door 14U Studio BK
269.0  (56.2%) Sennheiser EW-DX SK 3-pin Y1-3
55.0  (57.9%) Stork Studio Master Trumpet LDV6
32.0  (59.3%) Gewa CR Flesch Boxwood Germany
107.0  (59.8%) Sirus Quad R 470
119.0  (59.8%) Best Brass HR-7C French Horn GP
125.0  (59.8%) Rousseau Tenor Classic 4R
59.0  (60.2%) FBT VT-W3B
349.0  (60.3%) the box pro A 10 LA Flying Frame
799.0  (61.7%) Suzuki HB-7C Tonechimes
259.0  (61.8%) Super Light Oblong Violin Case 4/4 BU

Sort by biggest discount percentage, no Thomann brands:
53.0  (34.2%) Yellowtec Litt Signal Light YT9100 Base
105.0  (41.2%) Turbosound NUQ102-FY
1099.0  (42.0%) UNiKA NBB-1616e
185.0  (46.5%) LD Systems Roadman Slave
3398.0  (48.5%) JB-Lighting Sparx12 RGBW
339.0  (52.2%) Pearl Decade Maple Add-On Pack BB
24.0  (53.3%) Eurolite Diffuser Cover 40° Multiflood
269.0  (56.2%) Sennheiser EW-DX SK 3-pin Y1-3
55.0  (57.9%) Stork Studio Master Trumpet LDV6
32.0  (59.3%) Gewa CR Flesch Boxwood Germany
119.0  (59.8%) Best Brass HR-7C French Horn GP
125.0  (59.8%) Rousseau Tenor Classic 4R
59.0  (60.2%) FBT VT-W3B
799.0  (61.7%) Suzuki HB-7C Tonechimes
259.0  (61.8%) Super Light Oblong Violin Case 4/4 BU
555.0  (62.4%) Paiste Gong Stand 30" round
399.0  (62.4%) Drawmer CMC7
319.0  (63.9%) Bose Professional FreeSpace 3-II Flush Mt Bass B
119.0  (64.3%) JBL D8R2408
1049.0  (65.6%) Lakland Skyline 44-OS 4-String BK

Closeouts with rating:
Item(mfg='Ampeg', name='PF-350 Portaflex B-Stock', a_id=159556, price=333.0, num_ratings=1, avg_rating=10, category='GI')
Item(mfg='Guild', name='A-150 Savoy Blonde B-Stock', a_id=205315, price=1189.0, num_ratings=1, avg_rating=10, category='GI')
Item(mfg='Höfner', name='HCT-VTH-R Verythin Con B-Stock', a_id=76904, price=429.0, num_ratings=5, avg_rating=8, category='GI')
Item(mfg='Ignition', name='Tristan 200 B-Stock', a_id=455162, price=859.0, num_ratings=2, avg_rating=10, category='LI')
Item(mfg='MOTU', name='Audio Express B-Stock', a_id=162870, price=459.0, num_ratings=7, avg_rating=8.2857, category='CO')
Item(mfg='MXR', name='M134 Stereo Chorus B-Stock', a_id=54537, price=162.0, num_ratings=6, avg_rating=10, category='GI')
Item(mfg='Novation', name='Impulse 49 B-Stock', a_id=170439, price=211.0, num_ratings=7, avg_rating=9.1428, category='SY')
Item(mfg='Yamaha', name='CPX1200II TBL B-Stock', a_id=147535, price=1459.0, num_ratings=3, avg_rating=9.3333, category='GI')

Closeouts, some categories excl., no house brands, under 600:
[CO] Antelope Zen Q Synergy Core B-Stock - 555.0
[CO] MOTU Audio Express B-Stock - 459.0
[CO] Universal Audio UAD-2 Satellite Quad B-Stock - 389.0
[DR] DG De Gregorio Kanyero De Luxe Cajon B-Stock - 459.0
[DR] DW 12"x03" Performance Sn B-Stock - 277.0
[DR] Meinl 20" Byzance Tradit. Li B-Stock - 385.0
[DR] Meinl Woodcraft Pro Pickup C B-Stock - 156.0
[DR] Sabian 22" AAX Medium Ride B-Stock - 399.0
[HW] Global Truss F14R15-90 Circ. Elemen B-Stock - 159.0
[HW] Gravity FG SEAT 1 B-Stock - 98.0
[HW] Gravity MS 4221 B Microphone S B-Stock - 35.4
[HW] meychair SH-100 Stick Holder B-Stock - 24.2
[HW] Work AW-140B Truss Adapter B-Stock - 69.0
[MI] Bose Professional EdgeMax Ceiling Tile 6 B-Stock - 37.0
[MI] Hörluchs HL 4330 black B-Stock - 295.0
[MI] MXL AC-360-Z V2 Black B-Stock - 485.0
[MI] Rycote Wind Screen Kit 2 (MZL B-Stock - 535.0
[MI] SD Systems SDS MD Modular System B-Stock - 299.0
[MI] Shure BLX14/CVL T11 B-Stock - 325.0
[PA] Decksaver Rane Four B-Stock - 71.0
[PA] JBL 227H B-Stock - 215.0
[PA] LD Systems SAT 102 G2 B-Stock - 222.0
[PA] Radial Engineering PS4 Cherry Picker B-Stock - 399.0
[PA] Stanton STX B-Stock - 235.0
[PA] Tannoy VMS 1-WH B-Stock - 105.0
[ST] Audio-Technica BPHS2S B-Stock - 249.0
[ST] EVE Audio Mic Thread Mounting Br B-Stock - 42.0
[ST] Telex PH-44 Headset B-Stock - 289.0
[TR] Bergerault XACHC Xylophone Chrom. B-Stock - 115.0
[TR] Bound Free VN2028 Carbon Violin B B-Stock - 259.0
[TR] Conrad Götz ZK1596G-NAT Violin Chi B-Stock - 33.0
[TR] Gewa TH-70 Allegro Violin S B-Stock - 299.0
[TR] Metal Sounds Zenko Drum ZEN02 penta B-Stock - 325.0
```
