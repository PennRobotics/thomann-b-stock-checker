# Thomann B-Stock Checker

## Important Info

First thing's first:

### API Link

```
https://www.thomann.de/de/search_searchAjax.html?marketingAttributes[0]=BLOWOUT
```

&uarr; _**Please do not abuse this link.**_ &uarr;

### Takeaways

Great deals on audio gear at Thomann usually drop on Tuesday around noon, Central European Time.

High volume products (K&amp;M stands, Shure or Rode mics, Focusrite interfaces, Behringer mixers, DT-770s, Roland E-Drums, Yamaha keyboards) usually are discounted at least once a week.

Name brand percussion and wind instruments seems to be discounted less often than live audio gear like mics and stands. (This is probably related to volume.)

Some brands are virtually never discounted, such as Schoeps.

### Pipeline Shortcut

(for my own convenience) https://gitlab.com/PennRobotics/thomann-b-stock-checker/-/pipelines/new


## Usage

- Browse Thomann (the site, not the API) until you find products you want on sale
- ~Add the desired retail or B-stock item details to the file **wishlist.csv**~ <mark>(this is not yet implemented)</mark>
- Add known B-stock URLs (one per line) to **page-check.txt**
- Modify **scrape-api.py** to get result counts (and other info discoverable in the JSON schema) for specific manufacturers/categories/search terms

_and then either_

- Run the Gitlab pipeline job

_OR_

- Clone and use the pipeline configuration, **.gitlab-ci.yml**, as a guide to run scripts locally


## Inventory Patterns

![Plot of stock over time generated from CSV data](stock_timeline.png "Stock Timeline")


## Project Explanation

### Goal

Save money by buying B-stock items.

### Achievement

I created a wishlist in this repository and a process to check when any item on the wishlist has been discounted. Alternatively, whole product groups can be searched, such as one manufacturer's full product line. Thomann's list of discounted items is updated infrequently&mdash;something like once per day. A Gitlab action will check for the update and then add a data row with some collected data into a history file.

### Background

In general, people order from music stores and send stuff back for a variety of reasons: they discover they can't just plug a dynamic mic into a guitar amp, they are keeping the best option out of a handful of possibilities, their partner yelled at them for spending way too much money on hobbies instead of more important causes, etc.

When this happens, the store can no longer proclaim that a part is brand new, so they sell it at a small discount; perfectly good gear, but 5 to 15 percent cheaper than you'll find it anywhere else.

This&hellip; is **B-stock**.

For something like [Shure](https://www.shure.com/) microphones, there are simply too many [low-cost replicas](https://sine-post.co.uk/2017/11/10/are-you-sure/) (a.k.a. fakes) to risk buying used from an unknown seller. Also, Shure&mdash;like many brands&mdash;has a tight grip on the price set by each of their resellers even if this puts them at risk of litigation. (I'm lookin' at you, [Yamaha](https://www.yamaha.com/en/news_release/files/news/23011001/pdf/2301100101.pdf)&hellip;) Your best bet for a cheaper, authentic microphone&mdash;trusting the returner didn't mischievously swap in a fake and send that back&mdash;is B-stock.

Enough people want the Shure SM57 or SM58 that it is common to see one of these models had a B-stock item listed earlier in the day that had already sold out. As a counterpoint, high quality but expensive parts like the [DPA 4099](https://www.dpamicrophones.com/instrument/4099-instrument-microphone) tend to not get returned often, so unless you are checking every day, you might miss the only one for your instrument that gets returned in an entire quarter.

You can keep refreshing a search result or product page and hope to get lucky. (This is what I've done several times already.) You can cast a wide net and buy whatever is discounted in that category. (I've done this, too; sE makes some nice ribbon and SDC mics at their prices!) Or&hellip; We figure out a way to unobtrusively automate this process. This was the birth of an idea and this repository.

### Technical

Feel free to ask for clarification and to open an issue/pull request.

#### API discovery

_Inspect_ &rarr; _Network_ &rarr; _XHR_ shows the URL (above) used by https://www.thomannmusic.com/ to display inventory numbers via JSON. **scrape-api.py** uses this format to poll a specific subset of products, such as all discounted items by one manufacturer. If the numbers change, an inventory update definitely occurred, so the present UTC timestamp is saved to **LAST-RESTOCK-TIMESTAMP.txt**

#### How do we know it updated?

The update time seems to fluctuate each day. From what I've observed, there is only 1&ndash;2 updates per 1&ndash;2 days. A few days, I noticed stock changes around 10:00 or 10:30. After more observation, it seemed like the time was not so predictable. In fact, you often see a different selection on a brand's summary page than in the detailed product list around the time the update occurs. I also think there's occasionally a late evening refresh where all of the sold bargain items are cleared from the inventory list but no new bargain items are posted. In any case, updates are rare&mdash;probably once per day, and rolled out over an hour or so.

The response using the API link will be 200+ KB of JSON data. Navigate to **gtmSearchResponse** &rarr; **parameters** &rarr; **result_count**. On August 23rd in the evening, this value is _4019_.

> **Note**  
> It's possible Thomann caches the results it provides to a given IP address. The _result\_count_ that I see manually browsing the AJAX URL is different than the one seen by the latest pipeline run, which failed to detect an update. I'm not sure how this would happen or what consequence it has for this repository, and it also would change my idea of how Thomann's inventory is refreshed.

#### Recommendation: check small first

There's a small but non-zero chance you get the same value for the entire Thomann bargain inventory after a stock update. To catch this case, it's probably best to identify a few brands that have a high sales volume and low cost, as the odds of the total bargain stock plus several of these popular brands _all_ having repeat values after a stock update is far closer to impossible than just the total stock having a repeat value.

In fact, checking the popular brands first is my recommendation, because the staff at Thomann might become (rightly) pissed if you are downloading 10 MB of JSON every day just to catch the update wave. (If it matters to you, it seems they `LIMIT 100` results, but I don't know that this reduces their server load and/or bandwidth significantly.)

* [Shure](https://www.thomann.de/intl/search_searchAjax.html?marketingAttributes[]=BLOWOUT&oa=pra&manufacturer[]=Shure)
* [K&amp;M](https://www.thomann.de/intl/search_searchAjax.html?marketingAttributes[]=BLOWOUT&oa=pra&manufacturer[]=K%26M)
* [Yamaha](https://www.thomann.de/intl/search_searchAjax.html?marketingAttributes[]=BLOWOUT&oa=pra&manufacturer[]=Yamaha)
* [Fender](https://www.thomann.de/intl/search_searchAjax.html?marketingAttributes[]=BLOWOUT&oa=pra&manufacturer[]=Fender)

#### Why not test for inventory updates using the brands I want directly?

Ultimately, this depends on whether you want one specific thing or potentially dozens. I tend to browse generally for things that catch my eye once I know an update is posted, because who couldn't use another small-diaphragm omni mic or K&amp;M's latest product or a new release of a (non-Behringer) 32-key MIDI controller where the first key is F? (The latter being a compact form that is nearly perfectly aligned with a tenor trombone's full range; this one is going to become its [own Gitlab project](https://gitlab.com/PennRobotics/abc2pdf-action) in the future for directly recording MIDI to ABC format and then printing this as sheet music from a smartphone. I just need to get the [MIDI-to-ABC part](https://gitlab.com/PennRobotics/coxpiano/-/tree/main/lpk25) working.)

A less popular brand might only have 0 to 3 items discounted at any time with few interested buyers, so a full warehouse inventory update might not change how many items that manufacturer has. You might be looking for the top-of-the-line Latch Lake mic stand and a bunch of equally rare items, but since they usually have 10 products for weeks on end with nothing discounted. Using only Latch Lake to determine when an inventory refresh occurs is foolish.

A popular brand with 50 parts in the bargain selection will usually have 43 or 54 or 52 or 38 the next day. By filtering to specific manufacturers, a query will be smaller than if we request the entire B-stock inventory.

### Selenium

In the JSON data and on Thomann's site, retail and B-stock items have separate product IDs, which are all distinct from their backend data ID found only in the JSON. It's possible to identify a B-stock item by its JSON contents, but you need a reliable way of matching this item with its retail counterpart, and (short of product photo, which is _also_ unreliable) I haven't found the answer in the JSON data. While the B-stock URL is usually similar to the retail URL, it is only created the first time it is needed, and some B-stock URLs might have their product ID as part of the URL. Thus, it's not possible to guess what the current B-stock URL or ID will look like. It might be possible to get the B-stock link using the search bar AJAX URL, and this also does not seem fully reliable.

What works really well, then? When a B-stock item exists and is stocked, its product page eventually links to the retail item and the retail item links back to the B-stock equivalent. Selenium checks a list of retail URLs from **page-check.txt** and <mark>throws an error if a B-stock item **IS** found</mark>. I re-emphasize that finding a B-stock item throws an error, because it should be counterintuitive to have an error when the thing you want occurs. This error prompts Gitlab to send a _"pipeline failed"_ email&hellip;an easier way to send an email notice than Sendmail or anything else I can imagine.

After developing a way to detect the inventory refresh and deciding to only use JSON for targeted searches of product groups, I target specific products by creating **selenium-checks.py**. If **LAST-COMPLETE-GATHER-TIMESTAMP.txt** is older than the restock timestamp, this file is updated with the current timestamp and then a set of Selenium tests is executed.

Selenium, for those unaware, gives instructions to a web browser, e.g. look for a button, click the button, now look for a link, click the link.

> **Update:**
>
> Today, I noticed that the recently released and likely popular [Allen & Heath CQ18T](https://www.thomannmusic.com/intl/allen_heath_cq18t_b_stock.htm) was posted as a B-stock item. It sold out nearly immediately. If I were using ONLY the retail page to check if this had been listed, I would have missed it. It showed up in the JSON response. I don't have good advice on how I would have been able to get this deal. (In this specific case, the mixer will be on sale again later, but it's inevitable that some rare/valuable products will only go on sale once, ever. Maybe in that circumstance, the best course of action is to call the manufacturer and ask if they can cut you a deal.)

With the info already on this repository, it should be straightforward to figure out what works best for a particular need (searching specific items or swaths of products) and create a cron job to look for that query. All I ask is that you not abuse Thomann's site with the information here.

### Optimum Inventory Request Period

I only want to run expensive tests (Selenium) after knowing they won't be ineffective. Thus, I use timestamps and update them with cheap checks (Python _requests_ and Thomann's JSON results).

UTC timestamps are saved in files and pushed to this repository by storing a Git token in the CI configuration. I detail the token/git process in [another repository](https://gitlab.com/PennRobotics/test-self-commit-from-pipeline). This prevents excessive searching, since the Selenium script or targeted JSON searches should run only once per inventory refresh.

### Invalidating

I wanted to test if any of the (Gitlab) wishlist files changed so the checks can run again before the inventory update. I started implementing this but then figured a user would be using the current version of the webpage to add product info and can already check if a part is discounted. Anyone interested in this can probably implement it on their own. If not, post an issue and I'll get it fully functional.

### JSON

One detail that stumped me is how exactly the JSON response should be filtered in a way that targets specific known part numbers. Unless there's a field I'm missing, you cannot simply get an individual product's JSON using the search URL. You can use the general search and try an item number, and you will then get some results that might even include the B-stock title&mdash;this is _not_ guaranteed. If a manufacturer has more than 100 items discounted, the one you are searching is not guaranteed to be in the first set of JSON results. You can filter by price but then need to include this in your repository wishlist. In short, the search results JSON is quite good, and you might find a quirk or two that might affect your own search wishes.

I was hoping for progress using the [Shopping Basket](https://www.thomann.de/intl/basket.html) page, where you can type in an item number to add it directly to the cart. Using the same _XHR_ strategy as before, we find a basket AJAX page, but there is nothing on here that will link to B-stock when an A-stock item is put in the cart. There's also nothing definitive in the general search AJAX URL. I wasn't able to find other URLs that will yield JSON results and even checked the sitemap and **robots.txt**. This last file had a few references to "classified". Is this "Men In Black" classified? Or "senior citizen seeks companion for daily walks" classified?

[click here for the answer](https://www.thomann.de/intl/classified_search.html)

Spoiler: it's "newspaper" classifieds.

The **filterSettings** key in JSON results looked promising for searching individual IDs&mdash;particularly **articleNumbers**, but when I put this in the query, nothing happens. It seems these are unused, because when I include a manufacturer in the query, the **manufacturer** key is _null_.

When I open a B-stock item's JSON results: _articleListsSettings_ &rarr; _articles_ &rarr; _&lt;#&gt;_ &rarr; _aStockArticleId_

However, this isn't bi-directional. There is no flag on a retail article showing its "bStockArticleId". I imagine there are multiple reasons for discounting a product along with different price points, so it makes sense for every type of B-stock to link to its retail equivalent but not for a retail product to link to all possible B-stock IDs.

#### Rabbit Hole

The next step was to dive into the current revision of **thoapp.js** using the inspect debugger.

* 307 search hits for "ajax"
* 208 hits for ".html"
* 53 hits for "stock"

Starting with _stock_, I'm looking for anything related to "B-stock", so I'll check all 53 results quickly. Nothing immediately jumps out.

My last check is "seeother", which is the class that holds the link from a retail page to its B-stock counterpart and vice versa.

At this rate, it would be faster to just check each of the B-stock results, pagination included. I don't want to spend any more time looking around, and we already have multiple methods of automating our bargain hunt.


## File List

<table>
<tr><td>.gitlab-ci.yml</td><td>pipeline config: set up computer, automate commands that check Thomann's site</td></tr>
<tr><td>LAST-COMPLETE-GATHER-TIMESTAMP.txt</td><td>update after a Selenium job</td></tr>
<tr><td>LAST-COUNTS.txt</td><td>JSON sample of Thomann + 4 manufacturers and their discounted part counts</td></tr>
<tr><td>LAST-RESTOCK-TIMESTAMP.txt</td><td>update after **LAST-COUNTS.txt** changes</td></tr>
<tr><td>README.md</td><td>_You are here._</td></tr>
<tr><td>page-check.txt</td><td>**Put links to retail products here, which Selenium uses to check for B-stock availability**</td></tr>
<tr><td>plot.py</td><td>Generates a plot of the stock history, for manually saving as a graphic</td></tr>
<tr><td>scrape-api.py</td><td>Currently checks and updates LAST-COUNTS.txt</td></tr>
<tr><td>selenium-checks.py</td><td>Script for running Selenium checks</td></tr>
<tr><td>stock_timeline.png</td><td>Plot of selected stock history</td></tr>
<tr><td>update-history.txt</td><td>(TODO) will contain LAST-COUNTS.txt history</td></tr>
<tr><td>wishlist.csv</td><td>(TODO) spreadsheet of parts with known (retail or B-stock) IDs to check</td></tr>
</table>


## Asides

### URL parameters

It seems a mix of English and German is used for abbreviating attributes passed by URL. I have no idea what `cme=false` is, but `vf=true` would lead me to believe "Versand fertig" e.g. "In stock" or _ready to ship_. Categories are abbreviated like "PAMFZUST", which would be _PA_ &rarr; _Mikrofon_ &rarr; _Zubehor_ (accessories) &rarr; _Ständer_ (stands). Other times, the key is something like `feature-12345` or `manufacturer`.

In a few places, it's clear there is a translation issue, such as the differentiation between foam windscreens and "dead cat" windscreens. Give them a break, though; they search dozens of countries!

Tragically, some sub-categories have the same features as others (e.g. snare drum color) but the actual feature ID is different for each diameter of snare drum. Therefore, you can't search for a snare drum of a given color unless you first search for a specific drum diameter.

### Sales strategy

Really, Thomann should be sending targeted emails to people with products saved in their favorites/watch lists whenever one of those lands in the _Bargain_ category&mdash;especially if the customer is identified as a whale and those parts are high-cost, physically large, low sales volume products likely to take up extra space in the warehouse until they are sold again: large instruments, mixers, line array bundles, acoustic treatment, etc.

Then again, maybe this is a EU regulations minefield or maybe they've tried this and it turns off more customers than it ensnares?

I just know how I would react: For the most part, I would snatch up a great deal on any parts saved in my own Thomann wishlists until my budget runs dry.

**Also!** I would hope that anyone cloning this repository could check the last update time using the records stored in _my_ repository. This would keep the number of Thomann server requests as small as possible, which should make the Thomann staff quite pleased. Thomann has a fixed-size warehouse; their goal is (or _should be_) to reduce _inventory conversion period_, and one way to accomplish this is clearing out the bargain goods faster or more efficiently.

### If you really want something

Today, I ordered a part that I had my eye on for a while but was only getting listed once or twice per month. I knew the B-stock page URL and checked it before the inventory refresh e.g. before 10 o'clock. This item wasn't listed in any of the normal pages, but the product page showed the B-stock version was available, so I quickly added it to my cart and paid.

If you know a product that you urgently want at a discount, your best bet is to check the B-stock page itself. Keep it open in a tab and just refresh a few times a day or (to automate) set up a Selenium cron job as I've done before in this repository.

### Alternative Purchasing Strategy

If you notice a high-end item truly doesn't ever get returned or discounted (perfect example: Schoeps microphones), you simply have to buy elsewhere. Here's how that works:

- Skirt/break the law and buy it somewhere with no VAT and low/no duties and then bring it home without declaring
- Buy used
- If available, buy B-stock direct from the manufacturer. In the Schoeps case, you'll need a business tax ID; no idea if they accept a "Freiberufler"/"Entrepreneur" personal tax ID.
- Find a shop that does store-wide holiday discounts. Maybe a store in a neighboring European country does 10% off with free shipping only on Black Friday?


-----

For known B-stock pages where the product is sold out and was not recently stocked (e.g. a B-stock link saved to a Thomann wishlist) you can search for the B-stock item number in the HTML source. For instance, the DPA 4099 Drum microphone is item 453962, visible in the line `dataLayer.push({event:"gtmSoldOut", soldOutArtNr:"453962"})`

You can also search Google for cached results to get an idea of what the discounted price will be. In a few cases (cymbal sets, brass instruments) you might find a deal elsewhere that is cheaper than the B-stock price.


## Next steps

- [ ] add a few more specific queries
    - [ ] https://www.thomann.de/intl/latch_lake_accessories_for_stands.html?gk=ZUSTZS&cme=false&manufacturer%5B%5D=Latch%20Lake&bn=Latch%20Lake&filter=true (_gk_ and _manufacturer_ onto **search_dir.html**)
    - [ ] Sennheiser mics for toms
    - [ ] remove things obtained elsewhere or already (Silent Brass, Vater, Allen &amp; Heath)
    - [ ] let the wishlists be a guide
- [ ] plot data over long time as well as single week stats (SD, etc)
    - [ ] remove query for ALL and others that have been obtained
- [ ] check if there's some sort of relationship between a brand's number of available products, price distribution, major category, average rating, and percent discounted. We'll call this one, "Why aren't Schoeps products EVER on sale??" or perhaps "the Hidden Champions metric"
- [ ] include comment in pipeline where other, more knowledgeable/motivated users can inform themselves of stock updates e.g. `sendmail`
- [x] cron
    - [ ] Tweak the timing once a reliable pattern of inventory updates is discovered.
    - [ ] If the pattern is not super consistent, start long and then test frequent e.g. wait 12 hours, then 6, then 3, then 2 between tests. (This will eliminate up to 80% of hourly checks and should still yield pretty accurate results.)
    - [ ] see if Gitlab will hit a quota running containers on cron
- [ ] Figure out all Docker details
    - Use the Dockerhub-hosted Selenium images?
    - Create own image?
    - Mirror the Dockerhub image?
    - [ ] network usage vs quota
    - [ ] disk quota vs quota
    - [ ] possibility of caching filesystem layer somehow, if it helps
- [ ] Cache
    - As the CI pipeline uses Docker images and Python modules and seems to download them every time, there is probably a way to seriously reduce bandwidth by storing previously used files in the cache and reusing them. I don't really know how/if this works in Gitlab, and if it will actually affect stability or security.
- [ ] forkable client
    - [ ] create a repository recommended for cloning that uses mine as a submodule or reference (or cleverly apply some .gitignore or configuration flag) so that someone can generate their own product wishlist and queries but only after checking if an inventory refresh occurred using my repo's timestamp. This probably requires solving the JSON problem first, since this is probably the path of least bandwidth usage.
- [ ] CI improvements
    - [ ] Is there an appreciable difference between `dependencies` and `needs` in the CI file? Why did the second stage run once when it should depend on the first stage that had failed?
    - [ ] Make as much of the CI script DRY and variable-based as possible.
- [ ] Low importance
    - [ ] See if it's possible to have a checklist of options when running the pipeline manually (as GitHub can do)
        - At the very least, it's possible to have manual stages which can act as crude checkboxes.
    - [ ] slight file renames for better understandability (all caps vs title case vs lowercase for user-editable or auto-generated or none of the above)
